package com.puttysoftware.dbio;

import java.io.IOException;
import java.io.RandomAccessFile;

public class DatabaseIO implements AutoCloseable {
    // Fields
    private final RandomAccessFile raf;

    // Constructors
    public DatabaseIO(final String filename) throws IOException {
	this.raf = new RandomAccessFile(filename, "rw"); //$NON-NLS-1$
    }

    // Methods
    @Override
    public void close() throws IOException {
	this.raf.close();
    }

    public void writeByte(final int b) throws IOException {
	this.raf.writeByte(b);
    }

    public void writeShort(final int s) throws IOException {
	this.raf.writeShort(s);
    }

    public void writeInt(final int i) throws IOException {
	this.raf.writeInt(i);
    }

    public void writeLong(final long l) throws IOException {
	this.raf.writeLong(l);
    }

    public void writeBoolean(final boolean b) throws IOException {
	this.raf.writeBoolean(b);
    }

    public void writeString(final String s) throws IOException {
	this.raf.writeUTF(s);
    }

    public int readByte() throws IOException {
	return this.raf.readByte();
    }

    public int readShort() throws IOException {
	return this.raf.readShort();
    }

    public int readInt() throws IOException {
	return this.raf.readInt();
    }

    public long readLong() throws IOException {
	return this.raf.readLong();
    }

    public boolean readBoolean() throws IOException {
	return this.raf.readBoolean();
    }

    public String readString() throws IOException {
	return this.raf.readUTF();
    }
}
